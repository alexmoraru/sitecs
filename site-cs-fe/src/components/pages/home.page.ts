import { Component } from '@angular/core';

@Component({
  selector: 'home-page',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
})
export default class HomePage {
    private message:String;
    
    constructor() {
        this.message = "Home";    
    }
}