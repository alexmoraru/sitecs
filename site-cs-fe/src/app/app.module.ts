import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import HomePage from "../components/pages/home.page";

import Sidenav from "../components/layout/sidenav/sidenav.component";

@NgModule({
  declarations: [
    AppComponent,
    HomePage,

    Sidenav
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
